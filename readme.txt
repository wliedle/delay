

npm newman:
https://github.com/postmanlabs/newman
https://www.npmjs.com/package/newman

custom pipelines:
https://support.atlassian.com/bitbucket-cloud/docs/pipeline-triggers/
https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/#Configurebitbucket-pipelines.yml-ci_customcustom(optional)

newman commands
npm install newman -g
npm install newman-reporter-json-summary -g


# run simple collection:
newman run tests/"delay no env.postman_collection.json"

# run collection with environment:
newman run tests/"delay.postman_collection.json" -e tests/"delay localhost.postman_environment.json"

# alternative set environment vars with cli:
--env-var "<environment-variable-name>=<environment-variable-value>"

# specify iterations:
newman run tests/"delay.postman_collection.json" -e tests/"delay localhost.postman_environment.json" -n 4

# specify test data
newman run tests/"delay.postman_collection.json" -e tests/"delay localhost.postman_environment.json" -d tests/test_data_full.json

# specify  tests
newman run tests/"delay.postman_collection.json" -e tests/"delay localhost.postman_environment.json" -d tests/test_data_full.json --folder v2



newman run tests/"delay.postman_collection.json" -e tests/"delay localhost.postman_environment.json" -d tests/test_data_full.json --folder v2 -n 5 -r json-summary
