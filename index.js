const express = require("express");
const app = express();
const session = require("express-session");
const router = require("./routes");

const PORT = process.env.PORT || 85;
const SESSION_SECRET = "123qwe";

app.use(
  session({
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: {
      proxy: true,
      sameSite: false,
      secure: false,
      ephemeral: false,
      httpOnly: true,
    },
  })
);

app.use(express.json());

app.use(router);

app.listen(PORT, () => {
  console.log(`App listening on port: ${PORT}`);
});
