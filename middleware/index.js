const URL_KEY = "123qwe";

function checkKey(req, res, next) {
  if (req.query.key !== URL_KEY) {
    res.status(401).json({ message: "Please provide key!" });
  } else {
    next();
  }
}

module.exports = { checkKey };
