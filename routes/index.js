const router = require("express").Router();

const delay = require("./delay");
const echo = require("./echo");

router.get("/", (req, res) => {
  res.json({ message: "Running" });
});

router.use(delay);
router.use(echo);

module.exports = router;
