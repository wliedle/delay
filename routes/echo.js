const router = require("express").Router();

const BASE_ROUTE = "/echo";
const middleware = require("../middleware");

router.post(BASE_ROUTE, middleware.checkKey, (req, res) => {
  const message =
    req.body.message === undefined
      ? "Please provide message!"
      : req.body.message;

  res.json({ message });
});

module.exports = router;
