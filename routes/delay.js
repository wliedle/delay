const router = require("express").Router();

const BASE_ROUTE = "/delay";
const MAX_DELAY_IN_SECONDS = 200;
const middleware = require("../middleware");

router.get(BASE_ROUTE, (req, res) => {
  const key = req.query.key;

  if (key === undefined) {
    res.redirect(`${BASE_ROUTE}/0/`);
  } else {
    res.redirect(`${BASE_ROUTE}/0/?key=${req.query.key}`);
  }
});

router.get(`${BASE_ROUTE}/:delay`, middleware.checkKey, (req, res) => {
  const delaySeconds = req.params.delay;

  if (
    delaySeconds === undefined ||
    delaySeconds === null ||
    isNaN(delaySeconds) ||
    delaySeconds < 0 ||
    delaySeconds > MAX_DELAY_IN_SECONDS
  ) {
    res.status(400).json({
      message: "Provided delay is not valid.",
    });
  } else {
    const delayMilliseconds =
      delaySeconds === 0 ? delaySeconds : delaySeconds * 1000;

    setTimeout(() => {
      res.json({ delay: delaySeconds });
    }, delayMilliseconds);
  }
});

module.exports = router;
